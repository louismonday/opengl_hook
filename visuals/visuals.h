#ifndef VISUALS_H
#define VISUALS_H
#include <GL/gl.h>
class Visuals
{
public: //give our method a public access modifier
    Visuals();


    //declare our method
    int drawFPS();
    void BuildFonts();
};

#endif