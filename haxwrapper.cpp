#define _GNU_SOURCE
#include <dlfcn.h>
#include <stdlib.h>
//#include <GL/gl.h>

#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "visuals/visuals.h" //includes gl.h
static int framecnt = 0;

// our detour function
void doHookedGLOperations() {
	Visuals visuals;
	visuals.BuildFonts();
	framecnt++;
	printf("frame %d... \n", framecnt);
}


void glClear(GLbitfield mask) {
	static void (*lib_glClear)(GLbitfield mask) = NULL;
	void* handle;
	char* errorstr;

	if(!lib_glClear) {
		/* Load real libGL */
		handle = dlopen("/usr/lib/libGL.so", RTLD_LAZY);
		if(!handle) {
			fputs(dlerror(), stderr);
			exit(1);
		}
		/* Fetch pointer of real glClear() func */
		lib_glClear = dlsym(handle, "glClear");
		if( (errorstr = dlerror()) != NULL ) {
			fprintf(stderr, "dlsym fail: %s\n", errorstr);
			exit(1);
		}
	}

	/* hax */
	doHookedGLOperations();

	/* continue to actual libgl function*/
	lib_glClear(mask);
}
